# README #

1/3スケールのソニー HITBIT HB-55風小物のstlファイルです。 スイッチ類、I/Oポート等は省略しています。

組み合わせたファイルとパーツごとに配置したファイルの二種類があります。 元ファイルはAUTODESK Fusion360です。


***

# 実機情報

## メーカ
- ソニー

## 発売時期
- 1983年11月

## 参考資料

- [ボクたちが愛した、想い出のパソコン・マイコンたち](https://akiba-pc.watch.impress.co.jp/docs/column/retrohard/1084619.html)
- [コンピュータ博物館](http://museum.ipsj.or.jp/computer/personal/0100.html)

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_hitbit_hb-55/raw/3908699975427c9560d15b81cfb86030a520b607/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_hitbit_hb-55/raw/3908699975427c9560d15b81cfb86030a520b607/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_hitbit_hb-55/raw/3908699975427c9560d15b81cfb86030a520b607/ExampleImage.jpg)
